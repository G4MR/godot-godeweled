- allow player to switch tiles and check matches
    - switch tiles back on no matches
    - destroy tiles & tile branches and spawn tiles
- rebuild board grid when no more matches found
- keep track of score when matches are found
- remove start button

Polish:
- no more matches found text
    - add a timer on this
- music http://soundimage.org/puzzle-music/
- maybe add a start game scene and switch to it at the beginning


Left Off:
---------
Switching tiles animation isn't properly animating in the correct direction